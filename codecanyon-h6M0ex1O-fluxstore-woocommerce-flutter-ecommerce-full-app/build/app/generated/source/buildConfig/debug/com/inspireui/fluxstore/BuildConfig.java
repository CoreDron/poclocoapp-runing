/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.inspireui.fluxstore;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "com.inspireui.fluxstore";
  public static final String BUILD_TYPE = "debug";
  public static final int VERSION_CODE = 3021;
  public static final String VERSION_NAME = "2.2.0";
}
